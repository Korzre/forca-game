# Jogo da forca

O jogo da forca é um jogo em que o jogador tem que acertar qual é a palavra proposta, tendo como dica o número de letras e o tema ligado à palavra. A cada letra errada, é desenhado uma parte do corpo do enforcado

<img src="https://images-na.ssl-images-amazon.com/images/I/517cKCRsu8L.png" width="120px" height="120px">


Jogo da forca cliente-servidor que usa TCP para se comunicar através de soquetes de bloqueio. Construído usando Java 1.8.

- Para iniciar o cliene, vá para a pasta /Users/korzre/Documents/projetos eclipse/Hangman-Game-TCP-Sockets/Hangman/out/production/Hangman-Game-TCP-Sockets/server/net/ClientHandler.java
- Para iniciar o servidor, vá para a pasta /Users/korzre/Documents/projetos eclipse/Hangman-Game-TCP-Sockets/Hangman/out/production/Hangman-Game-TCP-Sockets/server/net/GameServer.java




Primeiro:servidor

Segundo: cliente