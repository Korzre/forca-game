package server.model;

class Game {
    private String word, gameWord, message;

    private static String startMsg = "Bemvindo ao jogo da Forca! Digita a palavra ou letra para adivinha. Digite 'Play Again' para reiniciar, ou 'End Game' para sair.";
    private static String loseMsg = "Perdeste!";
    private static String winMsg = "Ganhaste!";
    private static String inGameMsgCorrectGuess = "Correto!";
    private static String inGameMsgWrongGuess = "Errado!";
    private static String hiddenChar = "_";
    private int numAttempts, score = 0;
    private boolean wonGame;

    String startGame() {
        word = Word.getRandomWord();
        numAttempts = word.length();
        message = startMsg;
        wonGame = false;

        StringBuilder sb = new StringBuilder();

        for (char c: word.toCharArray()) {
            sb.append(hiddenChar);
        }

        gameWord = sb.toString();

        System.out.println("Word is " + word);

        return createResponse();
    }

    String gameEntry(String input) {
        boolean correctGuess = false;
        wonGame = false;

        if (numAttempts != 0) {
            if (input.length() > 1) {
                if (input.equals(word)) {
                    wonGame = true;
                    gameWord = word;
                }
            } else if (input.length() == 1) {
                StringBuilder sb = new StringBuilder();

                for (int i = 0; i < word.length(); i++) {
                    char c = word.charAt(i);
                    if (c == input.charAt(0)) {
                        sb.append(c);
                        correctGuess = true;
                    } else {
                        sb.append(gameWord.charAt(i));
                    }
                }

                gameWord = sb.toString();
                if (!gameWord.contains(hiddenChar))
                    wonGame = true;
            }

            if (correctGuess) {
                message = inGameMsgCorrectGuess;
            } else {
                message = inGameMsgWrongGuess;
                numAttempts--;
            }
        }

        if (wonGame) {
            return winGame();
        } else if (numAttempts < 1) {
            return loseGame();
        }

        return createResponse();
    }

    String restart() {
        return startGame();
    }

    private String loseGame() {
        message = loseMsg;
        score+=-1;
        return createResponse();
    }

    private String winGame() {
        message = winMsg;
        score++;
        return createResponse();
    }

    private String createResponse() {
        String response;

        StringBuilder sb = new StringBuilder();

        sb.append(score);
        sb.append("/");
        sb.append(numAttempts);
        sb.append("/");
        sb.append(gameWord);
        sb.append("/");
        sb.append(message);
        sb.append("/");

        response = sb.toString();
        return response;
    }
}
