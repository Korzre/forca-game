package server.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Word {
    private static ArrayList<String> lOfWords = new ArrayList<>();

    public static String getRandomWord(){
    	Path p = Paths.get("words.txt");
    	Path folder = p.getParent();
        try( Scanner sc = new Scanner(new File(folder.toString())))
        {
            while(sc.hasNext()){
                String str=sc.next();
                lOfWords.add(str);
            }
        } catch (FileNotFoundException e) {
            System.out.println("FILE IO ERROR: " + e.getMessage());
        }
        int index=new Random().nextInt(lOfWords.size());
        return lOfWords.get(index);
    }
}
