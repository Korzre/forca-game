package server.model;

public class GameSetup {
    private String gameData = "Game Data Default";
    private Boolean isGameStarted = false;
    private Game game;

    public String getGameData () {
        System.out.println("Server enviando a palavra: " + gameData);
        return gameData;
    }

    public void setGameData (String input) {
        System.out.println("Server recebendo a palavra: " + input);

        if (isGameStarted)
            switch (input) {
                case "start game":
                    this.gameData = "O jogo já está disponível";
                    break;
                case "play again":
                    restart();
                    break;
                default:
                    gameEntry(input);
                    break;
            }

        if (input.contains("start game") && !isGameStarted) {
            isGameStarted = true;
            startGame();
        }
    }

    private void startGame() {
        game = new Game();
        this.gameData = game.startGame();
    }

    private void restart() {
        this.gameData = game.restart();
    }

    private void gameEntry(String input) {
        this.gameData = game.gameEntry(input);
    }
}
